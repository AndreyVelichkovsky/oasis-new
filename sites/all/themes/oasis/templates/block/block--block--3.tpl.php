<div class="<?php print $classes; ?>"<?php print $attributes; ?> id="<?php print $block_html_id; ?>">
    <div class="wrap">
        <?php print render($title_prefix); ?>
        <?php if ($title): ?>
            <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
        <div class="wrap-content clearfix">
            <?php print $content; ?>
        </div>
    </div>
</div>
