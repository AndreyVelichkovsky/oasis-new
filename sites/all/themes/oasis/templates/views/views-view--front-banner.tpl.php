<?php if ($rows): ?>
    <section class="slider">
        <?php print $rows; ?>
    </section>
<?php elseif ($empty): ?>
    <div class="view-empty">
        <?php print $empty; ?>
    </div>
<?php endif; ?>