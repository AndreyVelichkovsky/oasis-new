<?php
if (isset($node->type) && $node->type == 'page') {
  if (!empty($node->field_page_subtitle)) {
    $title_header = 'header--title';
  }
}
else {
  $title_header = NULL;
}

?>
<header class="header <?php if (isset($title_header)) print $title_header; ?>">

    <?php
    if (isset($node->type) && $node->type == 'page') {
      $image = field_view_field('node', $node, 'field_header_image', 'full');

      print render($image);
    }
    ?>

  <?php if (isset($title_header)): ?>
      <div class="header__title-group">

  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
        <h1><?php print $title; ?></h1>
  <?php endif; ?>
  <?php print render($title_suffix); ?>


  <?php
  if (isset($node->type) && $node->type == 'page') {
    $subtitle = field_view_field('node', $node, 'field_page_subtitle', 'full');
    print render($subtitle);
  }
  ?>
      </div>
  <?php endif; ?>


<div class="header-top">
    <div class="header-top__wrapper">

    <div class="header-logo">
        <?php if ($logo): ?>
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="header__logo"><img
                    src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" class="header__logo-image"/></a>
        <?php endif; ?>
    </div>
    <div class="header-menu"><?php print render($page['header_top']); ?></div>

    </div>
</div>


    <?php print render($page['header']); ?>

</header>

<?php print render($page['highlighted']); ?>
<?php //print $breadcrumb; ?>
<?php //print render($title_prefix); ?>
<?php //if ($title): ?>
<!--    <h1>--><?php //print $title; ?><!--</h1>-->
<?php //endif; ?>
<?php //print render($title_suffix); ?>
<?php print $messages; ?>
<?php print render($tabs); ?>
<?php print render($page['help']); ?>
<?php if ($action_links): ?>
    <ul class="action-links"><?php print render($action_links); ?></ul>
<?php endif; ?>
<?php print render($page['content']); ?>
<?php print $feed_icons; ?>

<div class="footer">
    <div class="footer__wrapper">
        <div class="footer__logo">
            <a href="<?php print $front_page; ?>"><img
                    src="<?php print base_path() . path_to_theme() . '/logo-footer.png'; ?>"
                    alt="<?php print t('Home'); ?>"/></a>
        </div>
        <div class="footer__menu">
          <?php print render($page['footer']); ?>
        </div>


        <?php print render($page['footer_last']); ?>
    </div>
</div>
<?php print render($page['bottom']); ?>

