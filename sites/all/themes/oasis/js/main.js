(function ($) {
    $(document).ready(function () {

        $("#edit-newsletters .form-checkbox").attr('checked', true).hide();

        $("#edit-newsletters input[name='mail']").attr('placeholder', 'Email Address');

        $("#block-views-recent-news-block .view-content").slick({
            arrows: true,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            autoplay: true,
            responsive: [
                {
                    breakpoint: 830,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 621,
                    settings: {
                        slidesToShow: 1
                    }
                },
                {
                    breakpoint: 401,
                    settings: {
                        slidesToShow: 1,
                        arrows: false
                    }
                }
            ]
        });

        $(".slider").slick({
            speed: 600,
            //autoplay: true,
            autoplaySpeed: 2000,
            dots: true,
            fade: true,
            cssEase: 'linear',
            customPaging: function (slider, i) {
                var thumb = $(slider.$slides[i]).data('thumb');
                return '<span>' + thumb + '</a>';
            },
            responsive: [
                {
                    breakpoint: 802,
                    settings: {
                        arrows: false,
                        dots: true
                    }
                }
            ]
        });


        var el = $("#events-example"),
            log = $("#event-log");

        el.collapse()
            .bind("open", function (e, section) {
                log.html("'" + section.$summary.text() + "' was opened");
            })
            .bind("close", function (e, section) {
                log.html("'" + section.$summary.text() + "' was closed");
            });

    });
})(jQuery);