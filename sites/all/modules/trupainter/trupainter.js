/*
 * Author: dimonvv@gmail.com
 * Date: 05/01/2017
 * Version: 2.2
 */

	var _tp_Version = '2.2';
	var _tp_picWidth = 960; // width of the canvas
	var _tp_picHeight = 600; // height of the canvas
	var _tp_nLayer = 1;
	var _tp_picLength = _tp_picWidth * _tp_picHeight; // number of chunks
	var _tp_baseDir = '/sites/all/modules/trupainter/';
	var _tp_Layers = [ 
			{ src : _tp_baseDir + 'images/00.png', img : new Image() },
			{ src : _tp_baseDir + 'images/01.png', img : new Image() },
			{ src : _tp_baseDir + 'images/02.png', img : new Image() },
			{ src : _tp_baseDir + 'images/03.png', img : new Image() }
		]; // Create a new blank images.
	var _tp_maxLayers = _tp_Layers.length;
	var _tp_canvas;
	var _tp_Schemas = [
		{"schema":"Red", "colors":[
		{"name":"100%","color":"#ffffff"},
		{"name":"95%","color":"#ffe6e6"},
		{"name":"90%","color":"#ffcccc"},
		{"name":"85%","color":"#ffb3b3"},
		{"name":"80%","color":"#ff9999"},
		{"name":"75%","color":"#ff8080"},
		{"name":"70%","color":"#ff6666"},
		{"name":"65%","color":"#ff4d4d"},
		{"name":"60%","color":"#ff3333"},
		{"name":"55%","color":"#ff1a1a"},
		{"name":"50%","color":"#ff0000"},
		{"name":"45%","color":"#e60000"},
		{"name":"40%","color":"#cc0000"},
		{"name":"35%","color":"#b30000"},
		{"name":"30%","color":"#990000"},
		{"name":"25%","color":"#800000"},
		{"name":"20%","color":"#660000"},
		{"name":"15%","color":"#4d0000"},
		{"name":"10%","color":"#330000"},
		{"name":"5%","color":"#1a0000"}
//		,{"name":"0%","color":"#000000"}
	]},
		{"schema":"Cool Blue", "colors":[
		{"name":"100%","color":"#ffffff"},
		{"name":"95%","color":"#e6ecff"},
		{"name":"90%","color":"#ccd9ff"},
		{"name":"85%","color":"#b3c6ff"},
		{"name":"80%","color":"#99b3ff"},
		{"name":"75%","color":"#809fff"},
		{"name":"70%","color":"#668cff"},
		{"name":"65%","color":"#4d79ff"},
		{"name":"60%","color":"#3366ff"},
		{"name":"55%","color":"#1a53ff"},
		{"name":"50%","color":"#0040ff"},
		{"name":"45%","color":"#0039e6"},
		{"name":"40%","color":"#0033cc"},
		{"name":"35%","color":"#002db3"},
		{"name":"30%","color":"#002699"},
		{"name":"25%","color":"#002080"},
		{"name":"20%","color":"#001a66"},
		{"name":"15%","color":"#00134d"},
		{"name":"10%","color":"#000d33"},
		{"name":"5%","color":"#000613"}
//		,{"name":"0%","color":"#000000"}
	]},
		{"schema":"Tangy Orange", "colors":[
		{"name":"100%","color":"#ffffff"},
		{"name":"95%","color":"#fff5e6"},
		{"name":"90%","color":"#ffebcc"},
		{"name":"85%","color":"#ffe0b3"},
		{"name":"80%","color":"#ffd699"},
		{"name":"75%","color":"#ffcc80"},
		{"name":"70%","color":"#ffc266"},
		{"name":"65%","color":"#ffb84d"},
		{"name":"60%","color":"#ffad33"},
		{"name":"55%","color":"#ffa31a"},
		{"name":"50%","color":"#ff9900"},
		{"name":"45%","color":"#e68a00"},
		{"name":"40%","color":"#cc7a00"},
		{"name":"35%","color":"#b36b00"},
		{"name":"30%","color":"#995c00"},
		{"name":"25%","color":"#804d00"},
		{"name":"20%","color":"#663d00"},
		{"name":"15%","color":"#4d2e00"},
		{"name":"10%","color":"#331f00"},
		{"name":"5%","color":"#1a0f00"}
//		,{"name":"0%","color":"#000000"}
		]}
	];

	function _tp_Painter_Init ( divId = '_tp_Painter' ) {
//		alert(divId);
		jQuery('#' + divId).html('<ul style="padding:0; margin:0;"><li class="_tp_can" style="list-style-type:none;"><canvas id="_tp_Canvas" width="' + _tp_picWidth + '" height="' + _tp_picHeight + '">Use browser with canvas</canvas></li><li class="_tp_pick" style="list-style-type:none;"><div id="_tp_Picker"></div></li></ul>');

		canvas = document.getElementById("_tp_Canvas");
        if (canvas.getContext) {
          // Specify 2d canvas type.
			ctx = canvas.getContext("2d");
			_tp_Layers[0].img.onload = function() {
                ctx.drawImage(this, 0, 0);
			}
		}

		for ( var i = 0; i < _tp_maxLayers; i++ ) {
			_tp_Layers[i].img.src = _tp_Layers[i].src;
		}
		var html = '<ul style="padding:0; margin:0;">';
		for ( var i = 1; i < _tp_maxLayers; i++ ) html += '<li id="_tp_tabHead' + i + '" class="_tp_tabHead" onclick="_tp_SetLayer(' + i + ');">Color ' + i + '</li>';
		html += '<li style="list-style-type:none;clear:both;"></li></ul><div id="_tp_tabs"></div>';
 		jQuery('#_tp_Picker').html(html);
		html = '<ul style="padding:0; margin:0;">';
		for ( var i = 0; i < _tp_Schemas.length; i++ ) html += '<li id="_tp_subtabHead' + i + '" class="_tp_subtabHead" onclick="_tp_SetSubtab(' + i + ');">' + _tp_Schemas[i].schema + '</li>';
		html += '<li id="_tp_subtabHead' + i + '" class="_tp_subtabHead" onclick="_tp_SetSubtab(' + i + ');">Try Any Color</li>';
		html += '<li style="list-style-type:none;clear:both;"></li></ul><ul class="_tp_colors">';
		for ( var i = 0; i < _tp_Schemas.length; i++ ) {
			html += '<li style="list-style-type:none;"><div id="_tp_subtab' + i + '" class="_tp_subtab"><ul style="padding:0; margin:0;">';
			for (var j = 0; j < _tp_Schemas[i].colors.length; j++) {
				html += '<li title="' + _tp_Schemas[i].colors[j].name + '" class="_tp_colorPicker" style="background-color:' + _tp_Schemas[i].colors[j].color + '"></li>';
			}
			html += '</ul></div></li>';
		}
		html += '<li style="list-style-type:none;"><div id="_tp_subtab' + i + '" class="_tp_subtab">';
		html += '<p>Please download color code sheet </p><button id="_tp_btnDownload" onclick="_tp_clickDownload();">Download</button>';
		html += '<p>Put RGB color code here</p>';
		html += '<input type="text" id="_tp_r" name="_tp_r" placeholder="R" maxlength="3" size="3" min="0" max="255">';
		html += '<input type="text" id="_tp_g" name="_tp_g" placeholder="G" maxlength="3" size="3" min="0" max="255">';
		html += '<input type="text" id="_tp_b" name="_tp_b" placeholder="B" maxlength="3" size="3" min="0" max="255">';
		html += '<button id="_tp_btnAny" onclick="_tp_clickAny();">Apply</button></div></li></ul>';
 		jQuery('#_tp_tabs').html(html);
		jQuery('._tp_colorPicker').click(function(){
			var c = window.getComputedStyle(this).getPropertyValue('background-color');
//			alert(c);
			_tp_paintLayer(c);
		});
			
		_tp_SetLayer(1);
		_tp_SetSubtab(0);
	}
	
	function _tp_SetLayer(n) {
	    _tp_nLayer = n;
		jQuery('._tp_tabHead').removeClass('_tp_tabActive');
		jQuery('#_tp_tabHead' + n).addClass('_tp_tabActive'); 
	}	

	function _tp_SetSubtab(n) {
		jQuery('._tp_subtabHead').removeClass('_tp_subtabActive');
		jQuery('#_tp_subtabHead' + n).addClass('_tp_subtabActive'); 
		jQuery('._tp_subtab').hide();
		jQuery('#_tp_subtab' + n).show();
	}
	
	function _tp_validateRGB() {
		if (jQuery("#_tp_r").val() < 0) jQuery("#_tp_r").val(0);
		if (jQuery("#_tp_r").val() > 255) jQuery("#_tp_r").val(255);
		if (jQuery("#_tp_g").val() < 0) jQuery("#_tp_g").val(0);
		if (jQuery("#_tp_g").val() > 255) jQuery("#_tp_g").val(255);
		if (jQuery("#_tp_b").val() < 0) jQuery("#_tp_b").val(0);
		if (jQuery("#_tp_b").val() > 255) jQuery("#_tp_b").val(255);
	}

	function _tp_clickAny() {
		_tp_validateRGB();
		_tp_paintLayer("rgb(" + jQuery("#_tp_r").val() + ", " + jQuery("#_tp_g").val() + ", " + jQuery("#_tp_b").val() + ")");
	}

	function _tp_clickDownload() {
		_tp_validateRGB();
		alert("Color code is rgb(" + jQuery("#_tp_r").val() + ", " + jQuery("#_tp_g").val() + ", " + jQuery("#_tp_b").val() + ")");
	}

	function _tp_paintLayer(c) {
		var rgb = c.replace(/[^\d,]/g, '').split(',');

//		alert(rgb);
		var cnv, ctx, imgData;
		cnv = document.createElement('canvas');
		cnv.width = _tp_picWidth;
		cnv.height = _tp_picHeight;

		ctx = cnv.getContext('2d');
		ctx.drawImage(_tp_Layers[_tp_nLayer].img, 0, 0);
		
		imgData = ctx.getImageData(0, 0, cnv.width, cnv.height)
		data = imgData.data;
		for(var x = 0, len = data.length; x < len; x += 4)
		{
			if((data[x] > 0))
			{
				data[x] *= rgb[0] / 255;
				data[x + 1] *= rgb[1] / 255;
				data[x + 2] *= rgb[2] / 255;
			}

		}
		ctx.putImageData(imgData, 0, 0);
		if (canvas.getContext) {
			  // Specify 2d canvas type.
			ctx = canvas.getContext("2d");
			ctx.drawImage(cnv,0,0);
		}
	
	}

	var _tp_CalcItems = {"schema":"Quantity Calculator ", "items":[
			{"name":"Interior Emusions","coverage":"900"},
			{"name":"Exterior Emulsion - Smooth","coverage":"900"},
			{"name":"Exterior Emulsion - Texture","coverage":"100"},
			{"name":"Exterior Emulsion - Heritage","coverage":"100"},
			{"name":"Bathroom & Kitchen","coverage":"900"},
			{"name":"Anticarbonation","coverage":"900"},
			{"name":"Epoxy Primers","coverage":"1300"},
			{"name":"Epoxy Intermediates(TEXCOAT)","coverage":"150"},
			{"name":"Epoxy Top coats","coverage":"1200"},
			{"name":"Polyurethane Topcoats","coverage":"1200"},
			{"name":"Non Ferrous Primers","coverage":"800"},
			{"name":"Flame Ratrdant Coatings","coverage":"1200"}
		]}

	function _tp_Calculator_Init ( divId = '_tp_Calculator' ) {
//		alert(divId);
		var html = '';
		html += '<div class="_tp_section_wrapper"><div class="_tp_section"><label for="_tp_product">Product</label><select id="_tp_product" name="_tp_product">';
		for ( var i = 0; i < _tp_CalcItems.items.length; i++ ) 
			html += '<option value="' + i + '">' + _tp_CalcItems.items[i].name + '</option>';
		html += '</select></div>';
		html += '<div class="_tp_section"><label for="_tp_coverage">Practical Coverage</label><input type="text" value="' + _tp_CalcItems.items[0].coverage/100 + '" id="_tp_coverage" name="_tp_coverage"></div>';
		html += '<div class="_tp_section"><label for="_tp_area">Area in Sqr Meter</label><input type="text" value="1" id="_tp_area" name="_tp_area" autofocus></div>';
		html += '<div class="_tp_section"><button id="_tp_btnCalculate">Calculate</button></div></div>';
		html += '<p>Quantity in Liters: <span id="_tp_result"></span></p>';
		jQuery('#' + divId).html(html);
		jQuery('#_tp_btnCalculate').click(function(){
			jQuery("#_tp_result").text((jQuery("#_tp_area").val()*100/jQuery("#_tp_coverage").val()|0)/100);
		});
		jQuery('#_tp_product').change(function(){
			jQuery("#_tp_coverage").val(_tp_CalcItems.items[(jQuery("#_tp_product").val())].coverage/100);
			jQuery('#_tp_btnCalculate').click();
		});
		jQuery('#_tp_btnCalculate').click();
	}
	